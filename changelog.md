# Lib Change Log
* * * 
### Date:  2024-12-31
- V2.3.0
  - 软件触发ADC后清除软件触发寄存器；
  - 优化BKP实现，增加临界保护，调整寄存器位设置顺序，增加内部变量记录寄存器状态，避免重复设置；
  - 修改Can_m_FdClearIntFlag超时判断逻辑。

### Date:  2024-05-17
- V2.2.0
  - 新增deinit接口。
  - 修复库函数编译警告项。
  - CANFD发送仲裁模式选择为邮箱编号优先。
  - 优化清除DMA标志位。
  - 修改 “CMP滤波器滤波时钟分频” 数据类型。
  - 移除有关CACHE的函数接口。
  - USART增加设置波特率寄存器分母的限制。
  - 移除宏定义 `KF_INTERNAL_PRINT`。

### Date:  2024-01-16
  - 增加BETEN措施超时退出

### Date:  2023-12-15
  - 增加BETEN措施


### Date:  2023-09-23
  - *kf32a156_canfd.c*，`Can_m_FdCopyDataTo8MailBox`、`Can_m_FdCopyDataFrom8MailBox`等八个函数优化CANFD读写邮箱流程。

* * * 
### Date:  2023-09-12
  - *kf32a156.h*，根据《KF32A146_156用户手册_V2.1》（以下称手册）全面梳理各模块寄存器宏定义。
  - *kf32a156_adc.h, kf32a156_atim.h, kf32a156_btim.h, kf32a156_dac.h, kf32a156_gtim.h, kf32a156_i2c.h, kf32a156_int.h, kf32a156_osc.h, kf32a156_rtc.h, kf32a156_sysctl.h, kf32a156_usart.h, kf32a156_pm.h*， 根据手册梳理用于函数入参的宏定义、函数声明等内容。
  - *kf32a156_adc.c, kf32a156_atim.c, kf32a156_btim.c, kf32a156_cmp.c, kf32a156_dac.c, kf32a156_ecfgl.c, kf32a156_epwm.c, kf32a156_ewdt.c, kf32a156_fdc.c, kf32a156_flash.c, kf32a156_flexmux.c, kf32a156_gpio.c, kf32a156_gtim.c, kf32a156_i2c.c, kf32a156_int.c, kf32a156_osc.c, kf32a156_rtc.c, kf32a156_spi.c, kf32a156_pm.c*，根据手册梳理函数注释、函数定义、增删函数等内容。
  - **注意：具体修改内容见变更说明**

* * * 
### Date:  2023-07-10
  - *system_init.c*，`SetSysClock`函数新增操作备份域寄存器后关闭备份域操作。
  - *kf32a156_canfd.c*，`Can_m_FdControllerInit`新增操作备份域寄存器后关闭备份域操作。
  - *kf32a156_canfd.c*，修复`Can_m_FdCopyDataFrom8MailBox`、`Can_m_FdCopyDataFrom16MailBox`、`Can_m_FdCopyDataFrom32MailBox`、`Can_m_FdCopyDataFrom64MailBox`接收远程帧出现数据解析错误的问题。
  - *KF32A156.h*，修复`BTIM_SR_TXTDF_POS`、`BTIM_SR_TXUDF_POS`宏定义顺序错误。

### Date:  2023-05-18
  - *system_init.c*, *system_init.h*，固定`NOP_Delay_100us`函数优化等级。
  - *kf32a156_canfd.h*，添加宏定义注释。
  - *kf32a156_canfd.c*，写邮箱CODE后，增加对CODE的回读。

### Date:  2023-05-06
  - 默认使能备份域读写
  - 默认打开A02特性`SFR_SET_BIT_ASM(PM_CAL0,PM_CAL0_A01TOA02EN_POS);`

### Date:  2023-05-04
  - 修正`ECCP_Channel_Pin_Ctl`函数偏移错误问题
    - `~(ECCP_PXASCTL1_PXSS1L << (Port + Channel * 2))` --> `~(ECCP_PXASCTL1_PXSS1L << (Port + Channel * 4))`
  - 修正`USART_HalfDuplex_ClockPolarity_Config`函数逻辑错误
    - `if (NewState != USART_CPOL_FALL)` --> `if (NewState != USART_CPOL_RISE)`
  - 添加`systick_delay_us`和`systick_delay_ms`入口参数为0处理
    - 当入口参数为0时，直接返回
  - 恢复`SystemInit`函数中切换A02特性和CANFD全局变量相关语句
    - `SFR_SET_BIT_ASM(PM_CAL0,PM_CAL0_A01TOA02EN_POS);`
  - 主时钟切换为PLL时，添加2分频过程操作
    - ```c
      OSC_SCK_Division_Config(SCLK_DIVISION_2);
      OSC_SCK_Source_Config(SCLK_SOURCE_PLL);
      NOP_Delay_100us(2U, SCLK_Value / 2U);
      OSC_SCK_Division_Config(SCLK_DIVISION_1);
      NOP_Delay_100us(2U, SCLK_Value);
      ```

### Date:   2023-04-19
  修改BOR电压点选择
  修正`kf32a156.h`编码问题

### Date:   2023-04-17
  将抢占中断软件重发宏由默认关闭改为默认开启。`#define AIBITRATE_SOFTRETRANSMIT  STD_ON`。
  更改外部回环枚举变量名称。`CNAFD_LOOP_EXTERNAL_MODE`改为`CANFD_LOOP_EXTERNAL_MODE`

### Date:   2023-04-03
  USART发送判断标志由`USART_STR_TFEIF`改为`USART_STR_TFEIF1`  
  删除原有flash操作接口函数、宏定义等，现对flash的操作需统一使用IDE接口

### Date:   Mon Mar 20 2023 
  发送生效判断改为TXBSTA为0退出  

### Date:   Tue Mar 21 2023 
  修改宏变量名称，取消busoff恢复宏对busoff中断的使能控制  
  
### Date:   Mon Mar 20 2023 
  发送生效判断改为TXBSTA为0退出  
  
### Date:   Sat Mar 18 2023 
  更正API BUG  
  
### Date:   Tue Mar 7 2023 
  更改代码逻辑判断异常bug  
  
### Date:   Mon Feb 6 2023 
  为避免MailBox0发送失败，将写邮箱之后的软件仲裁操作删除  
  
### Date:   Tue Jan 31 2023 
  增加写邮箱之后的软件重新仲裁  
  
### Date:   Wed Jan 18 2023 
  增加持续DPram冲突错误状态返回操作  
  
### 2022-11-30 
* ★ V1.0.3
●1 修改SPI_CKE错误定义  
### 2022-11-30 
* ★ V1.0.2
●1 修改SPI_CKE错误定义  
### 2022-11-23
* ★ V1.0.1
●1 解决can_lib 中断设置BUG  
### 2022-08-04
* ★ V1.0.0
●1 基于A01标准外设库,变更芯片寄存器配置