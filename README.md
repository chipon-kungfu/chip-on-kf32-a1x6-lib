#### 文件所有

本文适用于KF32A156的模板工程配置的示例例程，例程文件名：KF32A156_StdPeriph_Template

* * *





#### 版权说明

目前的固件只是为了给使用者提供指导，目的是向客户提供有关产品的代码信息，

与使用者的产品信息和代码无关。因此，对于因此类固件内容和（或）客户使用此

处包含的与其产品相关的编码信息而引起的任何索赔，上海芯旺微电子技术有限公

司不承担任何直接、间接或后果性损害赔偿责任

* * *



#### 结构说明

本例展示了一个空的工程模板，主要由系统生成文件和标准外设库文件组成

##### 系统生成文件
* _config文件由系统生成，文件夹中包含两个文件vector.c和startup.c。vector.c中定义了
默认的中断函数名，startup.c中有一个startup()函数，该函数对变量进行初始化

* __Kungfu32_chipmodel_define.h文件由系统生成，其中包含型号定义，用户禁止修改其中
内容，否则会被覆盖或者导致程序编译失败

* kf_ic.c文件由系统生成,文件中提供了异常中断的处理模板，和中断服务程序的处理模板
例如，NMI、HardFault等中断的服务函数

* main.c文件由系统生成，是主程序文件，内容为空

##### 标准外设库文件
* src及inc文件夹为标准外设库文件

* system_init.c/.h为系统时钟配置文件

* * *

